let
  pkgs = import <nixpkgs> { };
in
pkgs.mkShell {
  
  packages = with pkgs; [
    nodejs
    pandoc
    htmldoc
    vscode-langservers-extracted
  ];
}
